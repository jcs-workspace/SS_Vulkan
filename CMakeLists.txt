# ========================================================================
# $File: CMakeLists.txt $
# $Date: 2023-04-09 01:23:16 $
# $Revision: $
# $Creator: Jen-Chieh Shen $
# $Notice: See LICENSE.txt for modification and distribution information
#                   Copyright © 2023 by Shen, Jen-Chieh $
# ========================================================================

cmake_minimum_required(VERSION 3.0)

# project settings
project(SS_Vulkan)
set(VERSION_MAJOR "1")
set(VERSION_MINOR "0")
set(VERSION_PATCH "0")
set(VERSION "${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}")

# environment settings
file(TO_CMAKE_PATH "$ENV{VULKAN_SDK}" VULKAN_SDK)

# subdir settings
add_subdirectory(src)
