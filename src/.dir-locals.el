;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((c++-mode . ((lsp-disabled-clients . (clangd))))
 (nil      . ((eval . (setq lsp-clients-clangd-library-directories
                            (list (expand-file-name "../libs/glfw/include/")
                                  (expand-file-name "../libs/glm/")
                                  (expand-file-name "../libs/imgui")
                                  (expand-file-name "include/" (getenv "VULKAN_SDK"))))))))

